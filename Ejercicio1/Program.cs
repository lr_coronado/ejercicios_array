﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio1
{
    class Program
    {
        static void Main(string[] args)
        {
            // Luis Rafael Coronado 2020-10295

            int[] arr = new int[4];
            int index = 0, sum = 0;

            do
            {
                Console.Write("Ingresa un numero: ");
                arr[index] = Convert.ToInt32(Console.ReadLine());
                index++;
            } while (index < 4);
            
            foreach (int x in arr) 
            { 
                Console.WriteLine(x);
                sum += x;
            }
            Console.WriteLine("La media es: {0}", sum/arr.Length);
        }
    }
}
