﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio5
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] nombres = new string[100];
            int count = 0;

            for (int i = 0; i < nombres.Length; i++) 
            {
                Console.WriteLine("Ingresa un nombre: ");
                string nombre = Console.ReadLine();
                if (nombre != "")
                {
                    nombres[i] = nombre;
                    count++;
                }
                else 
                {
                    break;
                }
            }

            Console.WriteLine("Los nombres introducidos son: ");
            for (int i = 0; i < count; i++) 
            {
                Console.WriteLine(nombres[i]);
            }
        }
    }
}
