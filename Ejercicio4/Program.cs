﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio4
{
    class Program
    {
        static void Main(string[] args)
        {
            int numero = 0, mayor = 0;
            int[] numeros = new int[10];

            for (int i = 0; i < 10; i++)
            {
                Console.Write("Ingresa un numero: ");
                numero = int.Parse(Console.ReadLine());
                numeros[i] = numero;
            }

            foreach (int x in numeros) 
            {
                if (x > mayor)
                {
                    mayor = x;
                }
            }

            Console.WriteLine("El Numero mayor es: {0}", mayor);
        }
    }
}
