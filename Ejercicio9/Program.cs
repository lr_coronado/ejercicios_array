﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio9
{
    class Multiplicacion 
    {
        public void DiujarTabla(int numero) 
        {
            for (int i = 0; i < 12; i++) 
            {
                Console.WriteLine($"{numero} X {i} = {numero * i} .");
            }
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Multiplicacion tabla = new Multiplicacion();
            int numero = 0;

            while (numero != -1) 
            {
                Console.Write("Ingresa un numero: ");
                numero = int.Parse(Console.ReadLine());
                if (numero == -1)
                {
                    break;
                }
                else 
                {
                    Console.WriteLine("La tabla del numero: {0}", numero);
                    tabla.DiujarTabla(numero);
                }
            }
        }
    }
}
