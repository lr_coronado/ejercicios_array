﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio3
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] diasDeMes = { 31,28,31,30,31,30,31,31,30,31,30,31 };

            Console.Write("Ingrese el numero del mes para obtener los dias: ");
            int index = int.Parse(Console.ReadLine());
            int posision = index - 1;
            switch (index)
            {
                case 1: Console.WriteLine("Enero tiene {0}", diasDeMes[posision]); break;
                case 2: Console.WriteLine("Febrero tiene {0}", diasDeMes[posision]); break;
                case 3: Console.WriteLine("Marzo tiene {0}", diasDeMes[posision]); break;
                case 4: Console.WriteLine("Abril tiene {0}", diasDeMes[posision]); break;
                case 5: Console.WriteLine("Mayo tiene {0}", diasDeMes[posision]); break;
                case 6: Console.WriteLine("Junio tiene {0}", diasDeMes[posision]); break;
                case 7: Console.WriteLine("Julio tiene {0}", diasDeMes[posision]); break;
                case 8: Console.WriteLine("Agosto tiene {0}", diasDeMes[posision]); break;
                case 9: Console.WriteLine("Septiembre tiene {0}", diasDeMes[posision]); break;
                case 10: Console.WriteLine("Octubre tiene {0}", diasDeMes[posision]); break;
                case 11: Console.WriteLine("Noviembre tiene {0}", diasDeMes[posision]); break;
                case 12: Console.WriteLine("Diciembre tiene {0}", diasDeMes[posision]); break;
                default:
                    Console.WriteLine("Ese no es un valor Valido.");
                    break;
            }
        }
    }
}
