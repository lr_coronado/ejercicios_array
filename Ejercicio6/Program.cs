﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio6
{
    class Fotografias 
    {
        public string nombre;
        public long anchoEnPixeles;
        public long altoEnPixeles;
        public float tamanoEnKb;
        public string ruta;
    }
    class Program
    {
        static void Main(string[] args)
        {
            Fotografias foto = new Fotografias();
            Fotografias[] fotos = new Fotografias[700];
            int count = 0;

            for ( ; ; )
            {
                Console.WriteLine("\n1- Agregar Imagen | 2- Ver Todas las Imagenes | 3- Buscar Imagen Por Nombre | 4- Salir");
                string opt = Console.ReadLine();
                if (opt == "1")
                {
                    if (fotos[699] != null)
                    {
                        Console.WriteLine("El almacenamiento esta lleno !.");
                    }
                    else 
                    {
                        Console.Write("Escribe el nombre: ");
                        string nombre = Console.ReadLine();

                        Console.Write("Escribe el Ancho En Pixeles: ");
                        long anchoEnPixeles = long.Parse(Console.ReadLine());

                        Console.Write("Escribe el Alto En Pixeles: ");
                        long altoEnPixeles = long.Parse(Console.ReadLine());

                        Console.Write("Escribe el Tamaño en KB: ");
                        float tamanoEnKb = float.Parse(Console.ReadLine());

                        Console.Write("Escribe la Ruta: ");
                        string ruta = Console.ReadLine();

                        Fotografias nuevaFoto = new Fotografias()
                        {
                            nombre = nombre,
                            anchoEnPixeles = anchoEnPixeles,
                            altoEnPixeles = altoEnPixeles,
                            tamanoEnKb = tamanoEnKb,
                            ruta = ruta
                        };

                        fotos[count] = nuevaFoto;
                        count++;
                    }
                }
                else if (opt == "2")
                {
                    int o = 0;

                    foreach (Fotografias x in fotos) { o++; }

                    for (int i = 0; i < o; i++)
                    {
                        try 
                        {
                            Console.WriteLine($"{i + 1} - {fotos[i].nombre}\n");
                        } 
                        catch (Exception) 
                        {
                        
                        }
                    }
                }
                else if (opt == "3")
                {
                    Console.Write("Escribe el nombre de la foto que quieres Buscar: ");
                    string fotoBuscar = Console.ReadLine();
                    foreach (Fotografias x in fotos)
                    {
                        try 
                        {
                            if (x.nombre == fotoBuscar)
                            {
                                Console.WriteLine($" - {x.nombre}, {x.anchoEnPixeles}, {x.altoEnPixeles}, {x.tamanoEnKb}, {x.ruta}");
                            }
                        }
                        catch(Exception)
                        {
                            
                        }
                    }
                }
                else if (opt == "4") 
                {
                    break;
                }
            }
        }
    }
}
