﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio2
{
    class Program
    {
        static void Main(string[] args)
        {
            float[] numeroReales = new float[5];
            
            for (int i = 0; i < 5; i++) 
            {
                Console.Write("Ingresa un numero: ");
                numeroReales[i] = Convert.ToSingle(Console.ReadLine());
            }

            Console.WriteLine("Array in reverse: ");
            foreach (float x in numeroReales.Reverse())
            {
                Console.WriteLine(x);
            }
        }
    }
}
