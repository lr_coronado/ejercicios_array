﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio8
{
    class Program
    {
        static void Main(string[] args)
        {
            float[] arr = new float[5];
            float sum = 0F, promedio = 0F, countMayores = 0F, countMenores = 0F;

            for (int i = 0; i < arr.Length; i++) 
            {
                Console.Write("Ingresa una altura: ");
                arr[i] = Convert.ToSingle(Console.ReadLine());
            }

            foreach (float x in arr) 
            {
                sum += x;
            }

            promedio = sum / arr.Length;

            foreach (float x in arr) 
            {
                if (x > promedio)
                {
                    countMayores++;
                }
                else 
                {
                    countMenores++;
                }
            }

            Console.WriteLine("El promedio de alturas es: {0}", promedio);
            Console.WriteLine("La cantidad de alturas Mayores del promedio: {0}", countMayores);
            Console.WriteLine("La cantidad de alturas Menores del promedio: {0}", countMenores);
        }
    }
}
