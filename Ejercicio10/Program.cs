﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio10
{
    class Persona 
    {
        public string nombre;
        public byte edad;

        public void ImprimirDatos() 
        {
            if (edad >= 18) 
            {
                Console.WriteLine("{0} tu edad es: {1}", nombre, edad);
            }
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Persona persona = new Persona();

            Console.Write("Escribe tu nombre: ");
            string nombre = Console.ReadLine();

            Console.Write("Escribe tu edad: ");
            byte edad = byte.Parse(Console.ReadLine());

            persona.nombre = nombre;
            persona.edad = edad;
            persona.ImprimirDatos();
        }
    }
}
